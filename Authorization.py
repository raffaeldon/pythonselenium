import unittest
import warnings

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
warnings.filterwarnings("ignore", category=DeprecationWarning)


class Authorization(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Opera()
        cls.driver.implicitly_wait(7)
        cls.driver.maximize_window()

    def setUp(self):
        self.driver.get("https://area.mtg-bi.com")

    def isSuccessSignIn(self, login, password):
        self.driver.find_element_by_xpath("//a[contains(text(), 'Sign IN')]").click()

        wait = WebDriverWait(self.driver, 7)

        signElement = wait.until(EC.element_to_be_clickable((By.ID, 'signin-login')))
        signElement.clear()
        signElement.send_keys(login)

        passElement = wait.until(EC.element_to_be_clickable((By.ID, 'signin-pass')))
        passElement.clear()
        passElement.send_keys(password)

        self.driver.find_element_by_xpath("//button[@type='button' and contains(text(), 'Sign IN')]").click()
        loginElems = self.driver.find_elements_by_id("signin-login")
        passElems = self.driver.find_elements_by_id("signin-pass")
        successLogIn = self.driver.find_elements_by_class_name("welcome-hello")

        if len(loginElems) != 0 and len(passElems) != 0 and len(successLogIn) == 0:
            return False
        else:
            return True

    # 1.1 Check links of buttons 'Sign in'
    def test_sign_in_links(self):
        result = False

        elems = self.driver.find_elements_by_xpath("//a[contains(text(), 'Sign IN')]")
        links = [elem.get_attribute('href') for elem in elems]

        if len(links) > 0:
            result = all(elem == links[0] for elem in links)

        assert result, "Different links of elements with text 'Sign In' (1.1)"
        # print("\nTest case 1.1 passed successfully!")

    # 1.2.1 Check blank fields (email/phone and password)
    def test_sign_in_blank_fields(self):
        assert not self.isSuccessSignIn("", ""), \
            "Authorization with blank field (login and pass) was success! (1.2.1)"
        # print("\nTest case 1.2.1 passed successfully!")

    # 1.2.2 Check black password
    def test_sign_in_blank_password(self):
        assert not self.isSuccessSignIn("testcaseqa3@gmail.com", ""), \
            "Authorization with blank password was success! (1.2.2)"
        # print("\nTest case 1.2.2 passed successfully!")

    # 1.2.3 Check wrong password
    def test_sign_in_wrong_password(self):
        assert not self.isSuccessSignIn("testcaseqa3@gmail.com", "1234567"), \
            "Authorization with wrong password was success! (1.2.3)"
        # print("\nTest case 1.2.3 passed successfully!")

    # 1.2.4 Check blank login and wrong password
    def test_sign_in_blank_login(self):
        assert not self.isSuccessSignIn("", "12345"), \
            "Authorization with blank login and wrong password was success! (1.2.4)"
        # print("\nTest case 1.2.4 passed successfully!")

    # 1.2.5 Check wrong login and right password
    def test_sign_in_wrong_login_and_right_password(self):
        assert not self.isSuccessSignIn("еуыесфыуйф3\"пьфшдюсщь", "123456"), \
            "Authorization with wrong login and right password was success! (1.2.5)"
        # print("\nTest case 1.2.5 passed successfully!")

    # 1.2.6 Check wrong numeric login and right password
    def test_sign_in_wrong_numeric_login_and_right_password(self):
        assert not self.isSuccessSignIn("89215554433", "123456"), \
            "Authorization with wrong numeric login and right password was success! (1.2.6)"
        # print("\nTest case 1.2.6 passed successfully!")

    # 1.3.1 Check right credentials
    def test_right_sign_in(self):
        assert self.isSuccessSignIn("testcaseqa3@gmail.com", "123456"), \
            "Authorization with right credentials wasn't success! (1.3.1)"
        # print("\nTest case 1.3.1 passed successfully!")

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()


if __name__ == "__main__":
    unittest.main()
