import unittest

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class UpdateProfile(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Opera()
        cls.driver.implicitly_wait(7)
        cls.driver.maximize_window()

    def setUp(self):
        self.driver.get("https://area.mtg-bi.com")
        self.successSignIn()
        self.clickProfile()

    def successSignIn(self):
        self.driver.find_element_by_xpath("//a[contains(text(), 'Sign IN')]").click()

        wait = WebDriverWait(self.driver, 7)

        signElement = wait.until(EC.element_to_be_clickable((By.ID, 'signin-login')))
        signElement.clear()
        signElement.send_keys("testcaseqa3@gmail.com")

        passElement = wait.until(EC.element_to_be_clickable((By.ID, 'signin-pass')))
        passElement.clear()
        passElement.send_keys("123456")

        self.driver.find_element_by_xpath("//button[@type='button' and contains(text(), 'Sign IN')]").click()

    def logOut(self):
        self.driver.find_element_by_xpath("//*[@id='navbarNav']/ul/li[2]/div/button/span").click()
        self.driver.find_element_by_xpath("//*[@class='dropdown-item' and contains(text(), 'Logout')]").click()

    def clickProfile(self):
        self.driver.find_element_by_xpath("//*[@id='navbarNav']/ul/li[2]/div/button/span").click()
        self.driver.find_element_by_xpath("//*[@class='dropdown-item' and contains(text(), 'Profile')]").click()

    def updateProfile(self, setname, setemail, setcountry, setdialcode, setphone, setbusseq):
        name = self.driver.find_element_by_id("profile-name")
        while len(name.get_attribute('value')) != 0:
            name.send_keys(Keys.BACKSPACE)
        name.send_keys(setname)

        email = self.driver.find_element_by_id("profile-email")
        while len(email.get_attribute('value')) != 0:
            email.send_keys(Keys.BACKSPACE)
        email.send_keys(setemail)

        country = Select(self.driver.find_element_by_id('profile-country'))
        country.select_by_value(setcountry)

        dialCode = self.driver.find_element_by_xpath("//*[@type='tel' and @placeholder='Dial code']")
        while len(dialCode.get_attribute('value')) != 0:
            dialCode.send_keys(Keys.BACKSPACE)
        dialCode.send_keys(setdialcode)

        phone = self.driver.find_element_by_id("profile-phone")
        while len(phone.get_attribute('value')) != 0:
            phone.send_keys(Keys.BACKSPACE)
        phone.send_keys(setphone)

        busseq = Select(self.driver.find_element_by_id('profile-busseg'))
        busseq.select_by_value(setbusseq)

        self.driver.find_element_by_xpath("//button[@type='button' and contains(text(), 'Update')]").click()

    def checkUpdatePermission(self):
        updatePermission = self.driver.find_elements_by_xpath(
            "//*[contains(text(), 'Account data successfully updated') or contains(text(), 'There is no any changes were found')]")

        return updatePermission

    def compareFields(self, name, email, country, dialcode, phone, busseq):
        currentName = self.driver.find_element_by_id("profile-name").get_attribute('value')
        currentEmail = self.driver.find_element_by_id("profile-email").get_attribute('value')
        currentCountry = self.driver.find_element_by_id('profile-country').get_attribute('value')
        currentDialCode = self.driver.find_element_by_xpath("//*[@type='tel' and @placeholder='Dial code']").get_attribute(
            'value')
        currentPhone = self.driver.find_element_by_id("profile-phone").get_attribute('value')
        currentBusseq = self.driver.find_element_by_id('profile-busseg').get_attribute('value')

        if currentName != name or currentEmail != email or currentCountry != country or currentDialCode != dialcode or \
                currentPhone != phone or currentBusseq != busseq:
            return False
        else:
            return True

    def updateAndCheck(self, name, email, country, dialcode, phone, busseq):
        self.updateProfile(name, email, country, dialcode, phone, busseq)

        updatePermission = self.checkUpdatePermission()

        self.logOut()
        self.successSignIn()
        self.clickProfile()

        compareFields = self.compareFields(name, email, country, dialcode, phone, busseq)

        return updatePermission and compareFields

    # 2.1 Check default params of field in profile
    def test_update_with_default_params(self):
        self.driver.find_element_by_xpath("//button[@type='button' and contains(text(), 'Update')]").click()
        updatePermission = self.checkUpdatePermission()

        assert not updatePermission, "Success update but nothing was changed (2.1)"

        # print("\nTest case 2.1 passed successfully!")

    # 2.2.1.1 Check blank field 'Your name'
    def test_update_with_blank_name(self):
        name = ""
        email = "testcaseqa3@gmail.com"
        country = "ES"
        dialcode = "+352"
        phone = "456456453"
        busseq = "13"

        assert not self.updateAndCheck(name, email, country, dialcode, phone, busseq), \
            "Success update but field 'Your name' is empty (2.2.1.1)"

        # print("\nTest case 2.2.1.1 passed successfully!")

    # 2.2.1.2 Check wrong field 'Your name'
    def test_update_with_wrong_name(self):
        name = "ыы"
        email = "testcaseqa3@gmail.com"
        country = "ES"
        dialcode = "+352"
        phone = "456456453"
        busseq = "13"

        assert not self.updateAndCheck(name, email, country, dialcode, phone, busseq), \
            "Success update but field 'Your name' is too short (2.2.1.2)"

        # print("\nTest case 2.2.1.2 passed successfully!")

    # 2.2.2.1 Check right field 'Your name'
    def test_update_with_right_name(self):
        name = "test case qa user"
        email = "testcaseqa3@gmail.com"
        country = "ES"
        dialcode = "+352"
        phone = "456456453"
        busseq = "13"

        assert self.updateAndCheck(name, email, country, dialcode, phone, busseq), \
            "Was not success update with right field 'Your name' (2.2.2.1)"

        # print("\nTest case 2.2.2.1 passed successfully!")

    # 2.3.1.1 Check blank field 'email'
    def test_update_with_blank_email(self):
        name = "test case qa user"
        email = ""
        country = "ES"
        dialcode = "+352"
        phone = "456456453"
        busseq = "13"

        assert not self.updateAndCheck(name, email, country, dialcode, phone, busseq), \
            "Success update with blank field 'email' (2.3.1.1)"

        # print("\nTest case 2.3.1.1 passed successfully!")

    # 2.3.1.2 Check wrong field 'email'
    def test_update_with_wrong_email(self):
        name = "test case qa user"
        email = "2@ы"
        country = "ES"
        dialcode = "+352"
        phone = "456456453"
        busseq = "13"

        assert not self.updateAndCheck(name, email, country, dialcode, phone, busseq), \
            "Success update with wrong field 'email' (2.3.1.2)"

        # print("\nTest case 2.3.1.2 passed successfully!")

    # 2.3.1.3 Check long and wrong field 'email'
    def test_update_with_long_and_wrong_email(self):
        name = "test case qa user"
        email = "789@0.8888ывапывапf90890890"
        country = "ES"
        dialcode = "+352"
        phone = "456456453"
        busseq = "13"

        assert not self.updateAndCheck(name, email, country, dialcode, phone, busseq),\
            "Success update with long and wrong field 'email' (2.3.1.3)"

        # print("\nTest case 2.3.1.3 passed successfully!")

    # 2.3.2.1 Check right field 'email'
    def test_update_with_right_email(self):
        name = "test case qa2 user"
        email = "testcaseqa3@gmail.com"
        country = "ES"
        dialcode = "+352"
        phone = "456456453"
        busseq = "13"

        assert self.updateAndCheck(name, email, country, dialcode, phone, busseq), \
            "Was not success update with right field 'email' (2.3.2.1)"

        # print("\nTest case 2.3.1.3 passed successfully!")

    # 2.4.1 Check blank field 'country'
    def test_update_with_blank_country(self):
        name = "test case qa2 user"
        email = "testcaseqa3@gmail.com"
        country = ""
        dialcode = "+352"
        phone = "456456453"
        busseq = "13"

        assert not self.updateAndCheck(name, email, country, dialcode, phone, busseq), \
            "Success update with blank field 'country' (2.4.1)"

        # print("\nTest case 2.4.1 passed successfully!")

    # 2.4.2 Check right field 'country'
    def test_update_with_right_country(self):
        name = "test case qa2 user"
        email = "testcaseqa3@gmail.com"
        country = "ES"
        dialcode = "+352"
        phone = "456456453"
        busseq = "13"

        assert self.updateAndCheck(name, email, country, dialcode, phone, busseq), \
            "Was not success update with right field 'country' (2.4.2)"

        # print("\nTest case 2.4.2 passed successfully!")

    # 2.5.1.1 Check blank field 'dial code'
    def test_update_with_blank_dial_code(self):
        name = "test case qa2 user"
        email = "testcaseqa3@gmail.com"
        country = "ES"
        dialcode = ""
        phone = "24546564004"
        busseq = "13"

        assert not self.updateAndCheck(name, email, country, dialcode, phone, busseq), \
            "Success update with blank field 'dial code' (2.5.1.1)"

        # print("\nTest case 2.5.1.1 passed successfully!")

    # 2.5.1.2 Check wrong field 'dial code'
    def test_update_with_plus_dial_code(self):
        name = "test case qa2 user"
        email = "testcaseqa3@gmail.com"
        country = "ES"
        dialcode = "+"
        phone = "68545568284"
        busseq = "13"

        assert not self.updateAndCheck(name, email, country, dialcode, phone, busseq), \
            "Success update with wrong field 'dial code' (2.5.1.2)"

        # print("\nTest case 2.5.1.2 passed successfully!")

    # 2.5.1.3 Check wrong field 'dial code'
    def test_update_with_wrong_dial_code(self):
        name = "test case qa2 user"
        email = "testcaseqa3@gmail.com"
        country = "ES"
        dialcode = "+1"
        phone = "457456845684"
        busseq = "13"

        assert not self.updateAndCheck(name, email, country, dialcode, phone, busseq), \
            "Success update with wrong field 'dial code' (2.5.1.3)"

        # print("\nTest case 2.5.1.3 passed successfully!")

    # 2.5.1.4 Check short field 'dial code'
    def test_update_with_short_dial_code(self):
        name = "test case qa2 user"
        email = "testcaseqa3@gmail.com"
        country = "ES"
        dialcode = "23"
        phone = "5775683"
        busseq = "13"

        assert not self.updateAndCheck(name, email, country, dialcode, phone, busseq), \
            "Success update with short field 'dial code' (2.5.1.4)"

        # print("\nTest case 2.5.1.4 passed successfully!")

    # 2.5.1.5 Check non-numeric field 'dial code'
    def test_update_with_non_numeric_dial_code(self):
        name = "test case qa2 user"
        email = "testcaseqa3@gmail.com"
        country = "ES"
        dialcode = "sdfadsf"
        phone = "првапрвапр"
        busseq = "13"

        assert not self.updateAndCheck(name, email, country, dialcode, phone, busseq), \
            "Success update with non-numeric field 'dial code' (2.5.1.5)"

        # print("\nTest case 2.5.1.5 passed successfully!")

    # 2.5.2.1 Check right field 'dial code' and 'phone'
    def test_update_with_right_dial_code(self):
        name = "test case qa2 user"
        email = "testcaseqa3@gmail.com"
        country = "ES"
        dialcode = "+3522"
        phone = "5775683"
        busseq = "13"

        assert self.updateAndCheck(name, email, country, dialcode, phone, busseq), \
            "Was not success update with right field 'dial code' and phone (2.5.2.1)"

        # print("\nTest case 2.5.2.1 passed successfully!")

    # 2.5.2.2 Check right field 'dial code' and 'phone'
    def test_update_with_right_dial_code_and_phone(self):
        name = "test case qa2 user"
        email = "testcaseqa3@gmail.com"
        country = "ES"
        dialcode = "+352"
        phone = "2841624123"
        busseq = "13"

        assert self.updateAndCheck(name, email, country, dialcode, phone, busseq), \
            "Was not success update with right field 'dial code' and phone (2.5.2.2)"

        # print("\nTest case 2.5.2.2 passed successfully!")

    # 2.5.2.3 Check right field 'dial code' and 'phone' with spaces
    def test_update_with_right_phone_with_spaces(self):
        name = "test case qa2 user"
        email = "testcaseqa3@gmail.com"
        country = "ES"
        dialcode = "+352"
        phone = "      2841624     "
        busseq = "13"

        assert self.updateAndCheck(name, email, country, dialcode, phone, busseq), \
            "Was not success update with right field 'dial code' and phone with spaces (2.5.2.3)"

        # print("\nTest case 2.5.2.3 passed successfully!")

    # 2.6.1 Check blank field 'Business segment'
    def test_update_with_blank_business_segment(self):
        name = "test case qa2 user"
        email = "testcaseqa3@gmail.com"
        country = "ES"
        dialcode = "+352"
        phone = "2841624123"
        busseq = ""

        assert not self.updateAndCheck(name, email, country, dialcode, phone, busseq),\
            "Success update with blank field 'Business segment' (2.6.1)"

        # print("\nTest case 2.6.1 passed successfully!")

    # 2.6.2 Check right field 'Business segment'
    def test_update_with_right_business_segment(self):
        name = "test case qa2 user"
        email = "testcaseqa3@gmail.com"
        country = "ES"
        dialcode = "+352"
        phone = "2841624123"
        busseq = "11"

        assert self.updateAndCheck(name, email, country, dialcode, phone, busseq), \
            "Was not success update with right field 'Business segment' (2.6.2)"

        # print("\nTest case 2.6.2 passed successfully!")

    def tearDown(self):
        self.logOut()

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()


if __name__ == '__main__':
    unittest.main()
