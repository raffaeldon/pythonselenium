import unittest
import Authorization
import UpdateProfile

testSuite = unittest.TestSuite()
testSuite.addTest(unittest.makeSuite(Authorization.Authorization))
testSuite.addTest(unittest.makeSuite(UpdateProfile.UpdateProfile))

runner = unittest.TextTestRunner(verbosity=2)
runner.run(testSuite)